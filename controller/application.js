import Application from "../model/Application.js"

export const myApplication = async (req,res)=>{
    const application = await Application.find({user:req.user._id}).populate(['job','user']).exec()
    res.send({message:"my application",data:application})
}
export const createApplication = async (req,res)=>{
    console.log(req.user)
    const application = await Application({
        user:req.user._id,
        job:req.body.job,
        linkedin:req.body.linkedin,
        portofolio:req.body.portofolio,
        additional_information:req.body.additional_information ? req.body.additional_information : ''
    })
    await application.save();
    res.send({message:"application created"})
}
