import Job from "../model/Job.js"

export const createJob = async(req,res)=>{
    const job = await Job({
        name:req.body.name,
        agency:req.body.agency,
        category:req.body.category,
        skill:req.body.skill
    })
    await job.save()
    res.send("job created")
}
export const job = async(req,res)=>{
    const job = await Job.find().exec();
    res.send(job)
}
export const updateJob = async(req,res)=>{
    const job = await Job.updateOne({_id:req.body.id,},{name:req.body.name})
    
    const jobUpdated = await Job.findOne(
        {_id:req.body.id,agency:req.body.agency}
        ).exec()
    res.send({message:"job updated",data:jobUpdated})
}
export const deleteJob = async(req,res)=>{
    const job = await Job.deleteOne({_id:req.body.id})
    res.send({message:"job deleted",id:req.body.id})
}
export const getJobById = async(req,res)=>{
    console.log(req.params)
    const job = await Job.findById(req.params.id).exec();
    res.send(job)
}