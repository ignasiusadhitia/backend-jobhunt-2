import express from 'express'
import { profile } from '../controller/user.js';
import { createApplication, myApplication } from '../controller/application.js';
import { createJob, deleteJob, getJobById, job, updateJob } from '../controller/job.js';
const router = express.Router();

router.post('/profile',profile)
router.get('/my-application',myApplication)
router.post('/create-application',createApplication)
router.post('/create-job',createJob)
router.get('/job',job)
router.get('/job/:id',getJobById)
router.post('/update-job',updateJob)
router.post('/delete-job',deleteJob)
export default router;